# Indonesian translation for tepl.
# Copyright (C) 2017 tepl's COPYRIGHT HOLDER
# This file is distributed under the same license as the tepl package.
# Andika Triwidada <atriwidada@gnome.org>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: tepl master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/amtk/issues\n"
"POT-Creation-Date: 2020-09-10 12:48+0200\n"
"PO-Revision-Date: 2017-12-05 14:52+0700\n"
"Last-Translator: Kukuh Syafaat <syafaatkukuh@gmail.com>\n"
"Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.0.4\n"

#. Translators: %s is a filename.
#: amtk/amtk-application-window.c:374
#, c-format
msgid "Open “%s”"
msgstr "Membuka \"%s\""

#: amtk/amtk-application-window.c:630
msgid "Open _Recent"
msgstr "Buka Te_rkini"

#. Translators: %s is the application name.
#: amtk/amtk-application-window.c:633
#, c-format
msgid "Open a file recently used with %s"
msgstr "Membuka dokumen yang baru-baru ini dipakai dengan %s"

#~ msgid "_New"
#~ msgstr "_Baru"

#~ msgid "New file"
#~ msgstr "Berkas baru"

#~ msgid "New _Window"
#~ msgstr "_Jendela Baru"

#~ msgid "Create a new window"
#~ msgstr "Membuat jendela baru"

#~ msgid "_Open"
#~ msgstr "_Buka"

#~ msgid "Open a file"
#~ msgstr "Buka suatu berkas"

#~ msgid "_Save"
#~ msgstr "_Simpan"

#~ msgid "Save the current file"
#~ msgstr "Simpan berkas saat ini"

#~ msgid "Save _As"
#~ msgstr "Simp_an Sebagai"

#~ msgid "Save the current file to a different location"
#~ msgstr "Simpan berkas saat ini ke lokasi yang berbeda"

#~ msgid "_Undo"
#~ msgstr "Tak _Jadi"

#~ msgid "Undo the last action"
#~ msgstr "Batalkan aksi terakhir"

#~ msgid "_Redo"
#~ msgstr "_Jadi Lagi"

#~ msgid "Redo the last undone action"
#~ msgstr "Jadikan lagi aksi terakhir yang dibatalkan"

#~ msgid "Cu_t"
#~ msgstr "Po_tong"

#~ msgid "Cut the selection"
#~ msgstr "Potong bagian yang dipilih"

#~ msgid "_Copy"
#~ msgstr "Sali_n"

#~ msgid "Copy the selection"
#~ msgstr "Salin bagian yang dipilih"

#~ msgid "_Paste"
#~ msgstr "Tem_pel"

#~ msgid "Paste the clipboard"
#~ msgstr "Tempel papan klip"

#~ msgid "_Delete"
#~ msgstr "_Hapus"

#~ msgid "Delete the selected text"
#~ msgstr "Hapus teks yang dipilih"

#~ msgid "Select _All"
#~ msgstr "Pilih Semu_a"

#~ msgid "Select all the text"
#~ msgstr "Pilih semua teks"

#~ msgid "_Indent"
#~ msgstr "_Indentasi"

#~ msgid "Indent the selected lines"
#~ msgstr "Indentasi baris yang dipilih"

#~ msgid "_Unindent"
#~ msgstr "Hap_us indentasi"

#~ msgid "Unindent the selected lines"
#~ msgstr "Hapus indentasi baris yang dipilih"

#~ msgid "Open File"
#~ msgstr "Buka Berkas"

#~ msgid "_Cancel"
#~ msgstr "Ba_tal"

#~ msgid "Read-Only"
#~ msgstr "Hanya-Baca"

#, c-format
#~ msgid "Save changes to file “%s” before closing?"
#~ msgstr "Simpan perubahan ke berkas \"%s\" sebelum ditutup?"

#~ msgid "Close _without Saving"
#~ msgstr "Tutup _tanpa Menyimpan"

#~ msgid "_Save As…"
#~ msgstr "_Simpan Sebagai…"

#~ msgid "Unicode"
#~ msgstr "Unikode"

#~ msgid "Western"
#~ msgstr "Barat"

#~ msgid "Central European"
#~ msgstr "Eropa Tengah"

#~ msgid "South European"
#~ msgstr "Eropa Selatan"

#~ msgid "Baltic"
#~ msgstr "Baltik"

#~ msgid "Cyrillic"
#~ msgstr "Sirilik"

#~ msgid "Arabic"
#~ msgstr "Arab"

#~ msgid "Greek"
#~ msgstr "Yunani"

#~ msgid "Hebrew Visual"
#~ msgstr "Ibrani Visual"

#~ msgid "Turkish"
#~ msgstr "Turki"

#~ msgid "Nordic"
#~ msgstr "Nordik"

#~ msgid "Celtic"
#~ msgstr "Seltik"

#~ msgid "Romanian"
#~ msgstr "Rumania"

#~ msgid "Armenian"
#~ msgstr "Armenia"

#~ msgid "Chinese Traditional"
#~ msgstr "Cina Tradisional"

#~ msgid "Cyrillic/Russian"
#~ msgstr "Sirilik/Rusia"

#~ msgid "Japanese"
#~ msgstr "Jepang"

#~ msgid "Korean"
#~ msgstr "Korea"

#~ msgid "Chinese Simplified"
#~ msgstr "Cina Sederhana"

#~ msgid "Georgian"
#~ msgstr "Georgia"

#~ msgid "Hebrew"
#~ msgstr "Ibrani"

#~ msgid "Cyrillic/Ukrainian"
#~ msgstr "Sirilik/Ukraina"

#~ msgid "Vietnamese"
#~ msgstr "Vietnam"

#~ msgid "Thai"
#~ msgstr "Thai"

#~ msgid "Unknown"
#~ msgstr "Tidak Dikenal"

#~ msgid "['UTF-8', 'CURRENT', 'ISO-8859-15', 'UTF-16']"
#~ msgstr "['UTF-8', 'CURRENT', 'ISO-8859-15', 'UTF-16']"

#, c-format
#~ msgid "Conversion from character set “%s” to “%s” is not supported."
#~ msgstr "Konversi dari gugus karakter \"%s\" ke \"%s\" tak didukung."

#, c-format
#~ msgid "Could not open converter from “%s” to “%s”: %s"
#~ msgstr "Tidak dapat membuka pengubah dari \"%s\" menjadi \"%s\": %s"

#~ msgid "The input data contains an invalid sequence."
#~ msgstr "Data masukan memuat suatu urutan yang tidak valid."

#, c-format
#~ msgid "Error when converting data: %s"
#~ msgstr "Galat saat mengonversi data: %s"

#~ msgid "The input data ends with an incomplete multi-byte sequence."
#~ msgstr "Data masukan diakhiri dengan urutan multi-byte yang tidak lengkap."

#~ msgid "The input content ends with incomplete data."
#~ msgstr "Isi masukan berakhir dengan data yang tidak lengkap."

#, c-format
#~ msgid "Untitled File %d"
#~ msgstr "Berkas Tanpa Nama %d"

#, c-format
#~ msgid "The file is too big. Maximum %s can be loaded."
#~ msgstr "Berkas telalu besar. Maksimum %s dapat dimuat."

#~ msgid "It is not possible to detect the character encoding automatically."
#~ msgstr "Tak mungkin mendeteksi pengkodean karakter secara otomatis."

#~ msgid "The file is externally modified."
#~ msgstr "Berkas telah diubah secara eksternal."

#~ msgid "The buffer contains invalid characters."
#~ msgstr "Penyangga mengandung karakter tak valid."

#~ msgid "Error when loading the file."
#~ msgstr "Galat saat memuat berkas."

#~ msgid "Save File"
#~ msgstr "Simpan Berkas"

#~ msgid "Location:"
#~ msgstr "Lokasi:"

#~ msgid "Close file"
#~ msgstr "Tutup berkas"

#~ msgid "Error when saving the file."
#~ msgstr "Galat saat menyimpan berkas."

#~ msgid "Could not find the file “%s”."
#~ msgstr "Tak bisa temukan berkas \"%s\"."

#~ msgid "Please check that you typed the location correctly and try again."
#~ msgstr ""
#~ "Silakan periksa apakah lokasi yang Anda ketikkan benar dan coba lagi."

#~ msgid "Unable to handle “%s:” locations."
#~ msgstr "Tak bisa menangani lokasi \"%s:\"."

#~ msgid "The location of the file cannot be accessed."
#~ msgstr "Lokasi berkas tak dapat diakses."

#~ msgid "“%s” is a directory."
#~ msgstr "\"%s\" adalah direktori."

#~ msgid "“%s” is not a valid location."
#~ msgstr "\"%s\" bukan lokasi yang sah."

#~ msgid ""
#~ "Host “%s” could not be found. Please check that your proxy settings are "
#~ "correct and try again."
#~ msgstr ""
#~ "Host \"%s\" tidak dapat ditemukan. Silakan periksa apakah tatanan proksi "
#~ "Anda sudah benar dan coba lagi."

#~ msgid ""
#~ "Hostname was invalid. Please check that you typed the location correctly "
#~ "and try again."
#~ msgstr ""
#~ "Nama host tidak valid. Silakan periksa lokasi yg Anda tik dan coba lagi."

#~ msgid "“%s” is not a regular file."
#~ msgstr "\"%s\" bukan berkas reguler."

#~ msgid "Connection timed out. Please try again."
#~ msgstr "Koneksi melewati waktu tunggu. Silakan coba lagi."

#~ msgid "Unexpected error: %s"
#~ msgstr "Kesalahan yang tidak diduga: %s"

#~ msgid "_Retry"
#~ msgstr "_Ulangi"

#~ msgid "Edit Any_way"
#~ msgstr "Sunt_ing Saja"

#~ msgid ""
#~ "The number of followed links is limited and the actual file could not be "
#~ "found within this limit."
#~ msgstr ""
#~ "Cacah taut yang diikuti terbatas dan berkas sesungguhnya tak ditemukan "
#~ "dalam batas ini."

#~ msgid "You do not have the permissions necessary to open the file."
#~ msgstr "Anda tidak memiliki izin yang diperlukan untuk membuka berkas."

#~ msgid ""
#~ "Unable to detect the character encoding.\n"
#~ "Please check that you are not trying to open a binary file.\n"
#~ "Select a character encoding from the menu and try again."
#~ msgstr ""
#~ "Tidak bisa mendeteksi pengkodean karakter.\n"
#~ "Harap periksa bahwa Anda tidak mencoba membuka suatu berkas biner.\n"
#~ "Pilih suatu pengkodean karakter dari menu dan cobalah lagi."

#~ msgid "There was a problem opening the file “%s”."
#~ msgstr "Ada masalah ketika membuka berkas \"%s\"."

#~ msgid ""
#~ "The file you opened has some invalid characters. If you continue editing "
#~ "this file you could corrupt it.\n"
#~ "You can also choose another character encoding and try again."
#~ msgstr ""
#~ "Berkas yang Anda buka memiliki beberapa karakter yang tidak valid. Bila "
#~ "Anda melanjutkan menyunting berkas ini Anda bisa merusaknya.\n"
#~ "Anda juga dapat memilih pengkodean karakter yang lain dan mencoba lagi."

#~ msgid "Could not open the file “%s” using the “%s” character encoding."
#~ msgstr ""
#~ "Tak dapat membuka berkas \"%s\" menggunakan pengkodean karakter \"%s\"."

#~ msgid ""
#~ "Please check that you are not trying to open a binary file.\n"
#~ "Select a different character encoding from the menu and try again."
#~ msgstr ""
#~ "Harap periksa bahwa Anda tidak mencoba membuka suatu berkas biner.\n"
#~ "Pilih suatu pengkodean karakter lain dari menu dan cobalah lagi."

#~ msgid "Could not open the file “%s”."
#~ msgstr "Tidak bisa membuka berkas \"%s\"."
